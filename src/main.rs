extern crate actix;
extern crate futures;
extern crate actix_web;

mod epub;
mod notification;
mod web;

use actix::prelude::*;
use actix_web::{server::HttpServer, App, HttpResponse};

fn main() {
    let sys = actix::System::new("epub-hub");
    let manager: Addr<Syn, _> = notification::start();
    HttpServer::new(|| web::app())
        .bind("127.0.0.1:59080").unwrap()
        .start();
    sys.run();
}

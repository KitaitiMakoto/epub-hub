extern crate zip;
extern crate quick_xml as xml;

use std::str;
use std::string;
use std::io::{self, Read};
use self::zip::ZipArchive;
use self::zip::read::ZipFile;
use self::zip::result::ZipError;
use self::xml::events::Event;

const CONTAINER_PATH: &str = "META-INF/container.xml";
const NS_OPF: &[u8] = b"http://www.idpf.org/2007/opf";
const NS_DC: &[u8] = b"http://purl.org/dc/elements/1.1/";

#[derive(Debug)]
pub enum Error {
    ArchiveCannotOpen(ZipError),
    ContainerNotFound(ZipError),
    ContainerCannotRead(io::Error),
    FullpathNotFound(string::FromUtf8Error),
    PackageNotFound(String),
    PackageCannotOpen(ZipError),
    PackageCannotRead(io::Error),
    UniqueIdentifierIdNotFound(str::Utf8Error),
    ModifiedNotFound(xml::Error),
    UniqueIdentifierNotFound(xml::Error)
}

pub fn get_unique_identifier_and_modified<R: io::Read + io::Seek>(file: R) -> Result<(String, String), Error> {
    let mut zip = try!(ZipArchive::new(file).map_err(Error::ArchiveCannotOpen));

    let full_path = {
        let container = try!(zip.by_name(CONTAINER_PATH).map_err(Error::ContainerNotFound));
        try!(extract_full_path(container))
    };

    let index = try!((0..zip.len()).find(|i| {
        let file = zip.by_index(*i).unwrap();
        file.name_raw() == full_path.as_bytes()
    }).ok_or(Error::PackageNotFound(full_path.to_string())));

    let package = try!(zip.by_index(index).map_err(Error::PackageCannotOpen));
    extract_unique_identifier_and_modified(package)
}

fn extract_full_path(mut container: ZipFile) -> Result<String, Error> {
    let mut xml_str = String::new();
    try!(container.read_to_string(&mut xml_str).map_err(Error::ContainerCannotRead));
    let mut reader = xml::Reader::from_str(&xml_str);
    reader.expand_empty_elements(true);
    let mut buf = Vec::new();
    loop {
        match reader.read_event(&mut buf) {
            Ok(Event::Start(ref e)) => {
                if e.name() == b"rootfile" {
                    for attr in e.attributes() {
                        let attr = attr.unwrap();
                        if attr.key == b"full-path" {
                            return String::from_utf8(attr.value.to_vec()).map_err(Error::FullpathNotFound);
                        }
                    }
                }
            },
            Ok(Event::Eof) => break,
            _ => {}
        }
        buf.clear();
    }
    panic!("No package document path found");
}

fn extract_unique_identifier_and_modified(mut package: ZipFile) -> Result<(String, String), Error> {
    let mut xml_str = String::new();
    try!(package.read_to_string(&mut xml_str).map_err(Error::PackageCannotRead));
    let mut reader = xml::Reader::from_str(&xml_str);
    reader.expand_empty_elements(true);
    let mut uiid = String::new();
    let mut unique_identifier = String::new();
    let mut in_unique_identifier = false;
    let mut modified = String::new();
    let mut in_modified = false;
    let mut buf = Vec::new();
    let mut ns_buf = Vec::new();
    loop {
        match reader.read_namespaced_event(&mut buf, &mut ns_buf) {
            Ok((ref namespaced_value, Event::Start(ref e))) => {
                match (namespaced_value.unwrap(), e.local_name()) {
                    (NS_OPF,  b"package") => {
                        for attr in e.attributes() {
                            let attr = attr.unwrap();
                            if attr.key == b"unique-identifier" {
                                uiid = try!(str::from_utf8(&attr.value).map_err(Error::UniqueIdentifierIdNotFound)).to_string();
                            }
                        }
                    },
                    (NS_OPF, b"meta") => {
                        for attr in e.attributes() {
                            let attr = attr.unwrap();
                            if attr.key == b"property" && attr.value.as_ref() == b"dcterms:modified" {
                                in_modified = true;
                            }
                        }
                    },
                    (NS_DC, b"identifier") => {
                        for attr in e.attributes() {
                            let attr = attr.unwrap();
                            if attr.key == b"id" && attr.value == uiid.as_bytes() {
                                in_unique_identifier = true;
                            }
                        }
                    }
                    _ => {}
                }
            },
            Ok((ref namespaced_value, Event::End(ref e))) => {
                match (namespaced_value.unwrap(), e.local_name()) {
                    (NS_OPF, b"meta") => {
                        if in_modified {
                            in_modified = false;
                        }
                    },
                    (NS_DC, b"identifier") => {
                        if in_unique_identifier {
                            in_unique_identifier = false;
                        }
                    },
                    _ => {}
                }
            },
            Ok((_, Event::Text(ref e))) => {
                if in_modified {
                    modified = try!(e.unescape_and_decode(&reader).map_err(Error::ModifiedNotFound));
                };
                if in_unique_identifier {
                    unique_identifier = try!(e.unescape_and_decode(&reader).map_err(Error::UniqueIdentifierNotFound)).trim().to_string();
                }
            },
            Ok((_, Event::Eof)) => break,
            _ => {}
        }
        buf.clear();
    }
    Ok((unique_identifier, modified))
}

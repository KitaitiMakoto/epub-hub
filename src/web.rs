use actix_web::{App, HttpResponse, http};

pub fn app() -> App {
    App::new()
        .resource("/", |r| r.method(http::Method::POST).f(|_| HttpResponse::Ok()))
}

use actix::prelude::*;

pub struct Uri(String);

impl Message for Uri {
    type Result = String;
}

impl Uri {
    pub fn new(uri: String) -> Uri {
        Uri(uri.to_string())
    }
}

pub struct Manager;

impl Actor for Manager {
    type Context = Context<Self>;
}

impl Handler<Uri> for Manager {
    type Result = String;

    fn handle(&mut self, msg: Uri, _ctx: &mut Context<Self>) -> String {
        eprintln!("{}", msg.0);
        msg.0
    }
}

pub fn start() -> Addr<Syn, Manager> {
    Manager.start()
}
